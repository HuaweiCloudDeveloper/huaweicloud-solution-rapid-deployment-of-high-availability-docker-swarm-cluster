[TOC]

**解决方案介绍**
===============
该解决方案可以帮助用户在华为云弹性云服务器上轻松搭建Docker Swarm集群，Docker是开发人员和系统管理员使用容器开发、部署和运行应用程序的平台。借助Docker，用户可以用与管理应用程序相同的方式来管理基础架构。通过利用Docker的方法来快速交付，测试和部署代码，用户可以大大减少编写代码和在生产环境中运行代码之间的延迟。

解决方案实践详情页面地址：https://www.huaweicloud.com/solution/implementations/rapid-deployment-of-high-availability-docker-swarm-cluster.html

**架构图**
---------------
![架构图](./document/rapid-deployment-of-high-availability-docker-swarm-cluster.png)

**架构描述**
---------------
该解决方案会部署如下资源：

1.创建2台Linux弹性云服务器，部署在不同的可用区，搭建Docker主管节点和从管节点，负责集群控制面，进行诸如监控集群状态、分发任务至工作节点等操作。

2.创建3台Linux弹性云服务器，部署在相同可用区，搭建Docker从管节点和工作节点，从管节点负责集群控制面，进行诸如监控集群状态、分发任务至工作节点等操作，工作节点接收来自管理节点的任务并执行。

3.创建安全组，可以保护弹性云服务器的网络安全，通过配置安全组规则，限定云服务器的访问端口。

**组织结构**
---------------

``` lua
huaweicloud-solution-rapid-deployment-of-high-availability-docker-swarm-cluster
├── rapid-deployment-of-high-availability-docker-swarm-cluster.tf.json -- 资源编排模板
├── userdata
    ├── docker_install01.sh  -- docker swarm主管节点配置文件
    ├── docker_install02.sh  -- docker swarm从管节点配置文件
    ├── docker_install03.sh  -- docker swarm工作节点配置文件
```
**开始使用**
---------------
***docker集群验证***

1.登录[ECS弹性云服务器](https://console.huaweicloud.com/ecm/?agencyId=8f3a7568dba64651869aa83c1b53de79&region=cn-north-4&locale=zh-cn#/ecs/manager/vmList)控制平台，选择{ecs_name}-manager01主管理节点的弹性云服务器，单击远程登录，或者使用其他的远程登录工具进入Linux弹性云服务器。

**图 1** 登录ECS云服务器控制平台
![登录ECS云服务器控制平台](./document/readme-image-001.PNG)

**图 2** 登录Linux弹性云服务器
![登录Linux弹性云服务器](./document/readme-image-002.png)

2.在Linux弹性云服务中输入账号和密码后回车。

**图 3** 登录ECS弹性云服务器
![登录ECS弹性云服务器](./document/readme-image-003.PNG)

3.查询集群节点状态信息，输入：docker node ls。

 **图 4** 查看集群节点状态信息
![查看集群节点状态信息](./document/readme-image-004.PNG)

4.部署Nginx进行应用测试。

（1）创建Nginx服务，搜索镜像 ：docker search nginx。

**图 5** 搜索镜像
![搜索镜像](./document/readme-image-005.PNG)

（2）下载镜像 ：docker pull nginx。

**图 6** 下载镜像
![下载镜像](./document/readme-image-006.PNG)

**图 7** 查看镜像 docker images
![查看镜像](./document/readme-image-007.PNG)

（3）使用service命令启动Nginx：*docker service create -p 8888:80 --name xybdiy-nginx nginx*。

**图 8** 启动Nginx
![启动Nginx](./document/readme-image-008.PNG)

5.动态扩缩容，实现高可用。

（1）扩容两个服务：docker service update --replicas 2 xybdiy-nginx 或者docker service scale xybdiy-nginx=2，查看启动容器：docker service ls。

**图 9** 查看启动容器
![查看启动容器](./document/readme-image-009.PNG)

（2）查看容器信息：docker service ps xybdiy-nginx

**图 10** 查看启动容器信息
![查看启动容器信息](./document/readme-image-010.PNG)

（3）获取这两台节点机器的公网IP

**图 11** 获取公网IP
![获取公网ip](./document/readme-image-011.PNG)

（4）通过公网ip访问Nginx服务：http://公网ip:8888

**图 12**  访问Nginx
![访问Nginx](./document/readme-image-012.PNG)

----结束
