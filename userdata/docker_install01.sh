#!/bin/bash
yum install epel-release -y
yum clean all

sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce

{
  echo  "172.16.0.30    $1-manager01"
  echo  "172.16.0.31    $1-manager02"
  echo  "172.16.0.32    $1-manager03"
  echo  "172.16.0.33    $1-worker01"
  echo  "172.16.0.34    $1-worker02"
} >>/etc/hosts

systemctl enable docker
systemctl start docker

docker swarm init --advertise-addr 172.16.0.30 2>&1 | tee /tmp/token01.txt

docker swarm join-token manager 2>&1 | tee /tmp/token02.txt

sed -n 5p /tmp/token01.txt 2>&1 | tee /tmp/token1.txt
sed -n 3p /tmp/token02.txt 2>&1 | tee /tmp/token2.txt