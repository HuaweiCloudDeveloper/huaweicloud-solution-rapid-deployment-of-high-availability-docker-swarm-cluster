#!/bin/bash
yum install epel-release -y
yum clean all

sudo yum install -y yum-utils device-mapper-persistent-data lvm2
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install -y docker-ce

{
  echo  "172.16.0.30    $1-manager01"
  echo  "172.16.0.31    $1-manager02"
  echo  "172.16.0.32    $1-manager03"
  echo  "172.16.0.33    $1-worker01"
  echo  "172.16.0.34    $1-worker02"
} >>/etc/hosts

systemctl enable docker
systemctl start docker

yum -y install expect
/usr/bin/expect <<EOF
spawn scp root@$1-manager01:/tmp/token1.txt /etc
expect "Are you sure you want to continue connecting (yes/no)?"
send "yes\r"
expect "password:"
send "$2\r"
expect eof
exit
EOF
sleep 10
while true;do
status=$(whereis token1.txt)
echo $status
if [[ $status =~ /etc/token1.txt ]]
then
echo "successfully"
break
else
/usr/bin/expect <<EOF
spawn scp root@$1-manager01:/tmp/token1.txt /etc
expect "password:"
send "$2\r"
expect eof
exit
EOF
echo "wait 10s..."
sleep 10
fi
done

var1=$(cat < /etc/token1.txt)
$var1